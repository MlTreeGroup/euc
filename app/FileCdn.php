<?php
namespace app;

use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

class FileCdn
{
    protected $auth;
    protected $bucket;
    protected $domain;

    public function __construct()
    {
        $accessKey = \env('CDN.QNACCESSKEY');
        $sercetKey = \env('CDN.QNSECRETKEY');

        $this->auth = new Auth($accessKey, $sercetKey);
        $this->bucket = \env('CDN.QNBUCKET');
        $this->domain = \env('CDN.QNDOMAIN');
    }

    public function put($fileURI)
    {
        $token = $this->auth->uploadToken($this->bucket);
        $uploadMgr = new UploadManager();
        return $uploadMgr->putFile($token, 'models/' . md5_file($fileURI) . '.ec', $fileURI);
    }
}

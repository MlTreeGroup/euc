<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class Models extends Model
{
    //
    protected $pk = 'model_id';

}

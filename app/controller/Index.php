<?php
declare (strict_types = 1);

namespace app\controller;

use app\BaseController;
use app\model\Models;
use think\facade\View;

class Index extends BaseController
{
    public function index()
    {
        $model_count = Models::count();
        $model_download_count = Models::sum('download_count');
        return view('index', [
            'model' => [
                'count' => $model_count,
                'download_count' => $model_download_count,
            ],
        ]);
    }

    public function search()
    {
        if ($this->request->isPost()) {
            $keyword = $this->request->post('keyword');
            $res = Models::where('name', 'like', '%' . $keyword . '%')
                ->select();
            View::assign('list', $res);
            View::assign('keyword', $keyword);
            return View::fetch();
        }
        return view();
    }

}

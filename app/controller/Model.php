<?php
declare (strict_types = 1);

namespace app\controller;

use app\BaseController;
use app\model\Models;
use think\Request;

class Model extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
        return;
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
        return view();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
        $data = $request->param();
        try {
            $this->validate($data, 'app\validate\Model');
        } catch (\Throwable $th) {
            return $this->out($th->getError(), [], -1);
        }
        $filePath = root_path() . '/runtime/storage/' . $data['fileName'];
        $fileHash = md5_file($filePath);

        $model = Models::getByHash($fileHash);
        if (!empty($model)) {
            return $this->out('该版本模块已存在！MD5：' . $fileHash, ['hash' => $fileHash], -2);
        }
        $upload = new \app\FileCdn();
        $res_cdn = $upload->put($filePath);

        if (!empty($res_cdn[0]['key'])) {
            $data['url'] = \env('CDN.QNDOMAIN') . $res_cdn[0]['key'];
        } else {
            return $this->out('error:CDN存储失败，请联系管理员处理!', [], -10);
        }

        $data['call_name'] = $data['call_name'] . "/" . $data['name'];
        $data['hash'] = md5_file($filePath);

        $res = Models::create($data);
        return $this->out('success');
    }

    public function upload()
    {
        if ($this->request->isPost()) {
            $file = $this->request->file('file');
            try {
                validate(['file' => 'fileExt:ec'])
                    ->check(['file' => $file]);

                $savename = \think\facade\Filesystem::putFile('models', $file, 'md5');
                if (empty($savename)) {
                    return $this->out('Model does excite!', [], -1);
                }
                return $this->out('success', $savename);

            } catch (\think\exception\ValidateException $e) {
                return $this->out($th->getMessage(), [], -1);
            }

        }
    }

    public function checkCallName()
    {
        if ($this->request->isPost()) {
            $call = $this->request->post('callname');
            $name = $this->request->post('name');

            $res = Models::getByCallName($call . '/' . $name);

            if (empty($res)) {
                return $this->out('名称可用');
            } else {
                return $this->out('名称重复');
            }
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
        $model = Models::getByHash($id);
        if (empty($model)) {
            return $this->out('empty', [], -1);
        }
        $model->download_count += 1;
        $model->save();

        return $this->out('success', $model);
    }

    public function find()
    {
        if ($this->request->isPost()) {
            $call = $this->request->post('call');
            $model = Models::where('call_name', $call)->order('version', 'desc')->select()[0];
            if (empty($model)) {
                return $this->out('mot found', [], -3);
            }
            if (!$this->request->has('version')) {
                return $this->out('success', $model);
            }
            $version = $this->request->post('version');
            $model = Models::where('call_name', $call)->where('version', $version)->find();
            if (empty($model)) {
                return $this->out('mot found', [], -3);
            }
            $model->download_count += 1;
            $model->save();

            return $this->out('success', $model);

        }
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}

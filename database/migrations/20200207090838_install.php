<?php

use think\migration\db\Column;
use think\migration\Migrator;

class Install extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('models', ['id' => 'model_id']);
        $table->addColumn('name', 'string', ['comment' => '模块名称'])
            ->addColumn('call_name', 'string')
            ->addColumn('hash', 'string')
            ->addColumn('version', 'string', ['limit' => '7'])
            ->addColumn('url', 'string')
            ->addColumn('email', 'string')
            ->addColumn('create_time', 'integer')
            ->addColumn('download_count', 'integer', ['default' => 0])
            ->create();
    }
}
